---
title: "Ñoquis Italianos (Capunti)"
date: "2022-01-12 13:26:00"
description: "Deliciosos Ñoquis al estilo Italiano"
type: "Salado"
tags: ["ñoquis","cena","pastas","harina","sano","almuerzo","salsa","comida"]
category: ["comida","pastas","sano","ñoquis","capunti","pasta"]
ingredientes: ["Harina","Sal","1 huevo (Opcional)","Aceita Oliva (Opcional)", "Agua Caliente"]
time: "50min"
img: "https://i.blogs.es/8fd9e7/1366_2000-1-/450_1000.jpg"
---

* Agregamos la harina en un recipiente con la sal a gusto.
* En el caso de querer poner huevo/s agregamos y comenzamos a mezclar.
* Agregamos el aceite y el agua caliente (No tiene que quemarnos pero mientras mas caliente mas suave la masa)
* Amasamos y estiramos con un palo de amasar hasta formar una masa uniforme y fina.
* Cortamos la masa en tiras de 2 o 3 dedos de ancho (Juntamos el índice, medio y anular y esa es la medida aproximada del ancho de las tiras)
* Cortamos la tiras en forma horizontal del ancho de un dedo quedando rectángulos pequeños como resultado.
* Le damos la forma como se muestra a continuación:

<br>
<div class="embed-responsive embed-responsive-16by9" style="border-radius:20px;">
  <iframe class="embed-responsive-item" src="https://www.minds.com/embed/1321478100298502100" frameborder="0" allowfullscreen="1"></iframe>
</div>
<br>
En el caso de no ver el vídeo puedes verlo desde su fuente original en el siguiente <a href="https://www.minds.com/newsfeed/1321478100298502150?referrer=patojad" target="_blank">enlace</a>
<br>
<br>

* Hervir durante 8 minutos aproximadamente (varia dependiendo de como quede la masa)
* Servir con salsa a elección.

<br>
<br>
<div class="embed-responsive embed-responsive-16by9" style="border-radius:20px;">
  <iframe class="embed-responsive-item" src="https://www.minds.com/embed/1321468271265845200" frameborder="0" allowfullscreen="1"></iframe>
</div>
<br>
En el caso de no ver el vídeo puedes verlo desde su fuente original en el siguiente <a href="https://www.minds.com/newsfeed/1321468271265845262?referrer=patojad" target="_blank">enlace</a>
