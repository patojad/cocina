---
title: "Empanadas con masa de espinaca y avena"
date: "2021-04-22 09:26:00"
description: "Deliciosas empanadas con masa de espinaca y avena"
type: "Salado"
tags: ["empanadas","cena","espinaca","avena","sano","almuerzo","cebolla","comida"]
category: ["comida","empanadas","sano"]
ingredientes: ["1 taza de espinaca picada, cocida y escurrida (10 cubitos de espinaca congelada)","2 tazas de harina de avena (la obtenés licuado/procesando la avena)","1 huevo","1/2 cta de sal","1 cta de polvo de hornear","ajo en polvo, mostaza en polvo, pimienta (condimentos a gusto)"]
time: "50min"
img: "https://i.postimg.cc/T3TBGBXc/image.png"
---

* Mezclar todos los ingredientes en un bol hasta obtener un bollo que no se pega.
* Colocar la masa entre 2 capas de papel de film y estirar con un palo.
* Cortar la masa en círculos de 13 cm de diámetro para las empanadas o forrar una asadera redonda para tarta.
* Rellenar a gusto puede usarse un salteado de:
  * calabaza
  * cebolla
  * orégano
  * Cubos de mozzarella
* Cocinar durante 30 min a 200ºC. Servir.
