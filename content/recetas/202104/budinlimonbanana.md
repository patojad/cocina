---
title: "Budín de banana"
date: "2021-04-16 09:26:00"
description: "Deliciosos Budín de 🍌"
type: "Dulce"
tags: ["budin","postre","banana","merienda","sano","postres","desayuno"]
category: ["postres","budin","sano"]
ingredientes: ["3 Bananas maduras medianas","2 tazas de avena","2 huevos grandes", "1/4 taza de miel", "1 cdita de bicarbonato de sodio"]
time: "55min"
img: "https://i.postimg.cc/wT8rtqWc/image.png"
---

Deliciosos Budín de 🍌 para una merienda o desayuno sano y delicioso

## Preparación

* Pisar las bananas.
* Integrar los huevos de a uno y batir hasta que queden bien integrados.
* Agregar la miel que tiene que estar ligera ( podes hacerlo con 1/2 taza de azúcar) luego el bicarbonato, la avena y al molde con Rocío vegetal.
* Si queres lo podes hacer en la licuadora o procesadora.
* Llévalo al horno por 45 min a 200º
