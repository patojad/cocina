---
title: "Nachos de harina de Maiz"
date: "2021-04-23 10:46:00"
description: "Deliciosos snacks de harina de maiz"
type: "Salado"
tags: ["nachos","postre","maiz","harina","merienda","sano","snack","desayuno"]
category: ["snack","nachos","sano"]
ingredientes: ["200 gramos de harina de maíz", "1 cucharadita de sal", "Aceite de oliva", "Agua", "Pimienta y pimentón dulce", "1 cucharada de harina de trigo para aglutinar", "50 gramos de queso rallado"]
time: "60min"
img: "https://cdn.bioguia.com/embed/94c1b5c5197fac9b96299f54d03c31524326464/Como_hacer_nachos_caseros_y_saludables?imagick=1&size=1250"
---

Prepara los tradicionales nachos de manera económica y saludable, para acompañar con la salsa que más te guste.

# Preparación

* Mezcla la harina de maíz y de trigo en un recipiente. Añade un chorrito de aceite de oliva y un poco de agua caliente. Sazona con sal y pimienta.
* Añade el queso y sigue agregando agua caliente hasta formar una masa.
* Pasa la mezcla a la mesada limpia. Amasa con las manos. Deja reposar por 10 minutos.
* Coloca el bollo de preparación entre dos trozos de film plástico (o mejor alguno de sus sustitutos sustentables) y aplástala con un palote. Luego, levanta el film y corta la masa en tiras rectangulares y después en pequeños triángulos.
* Tomando el film de abajo, levanta los triángulos de la mesada y colócalos en una fuente. Lleva al congelador por una hora.
* Retira. Lubrica una fuente con aceite, distribuye los triángulos y pincélalos con oliva.
* Hornea hasta que estén dorados, y condimenta con pimentón dulce, ciboulette, ¡o lo que más te guste!
