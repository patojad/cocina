---
title: "Tortilla para warp sin harina"
date: "2021-04-16 09:26:00"
description: "Deliciosas Tortilla para warp sin harina"
type: "Salado"
tags: ["tortilla","warp","harina","merienda","sano","almuerzo","desayuno","comida"]
category: ["comida","budin","sano"]
ingredientes: ["2 tazas de espinaca cruda","2 huevos o 2 tazas de espina cruda","1 huevo y 2 claras"]
time: "15min"
img: "https://i.postimg.cc/g2VyMRPr/image.png"
---

Tortilla para warp sin harina. La proteína la tiene la tortilla. Puede ser un almuerzo, cena o desayuno!⁣

## Preparación

* Licuar o micese.
* Cocinar el sartén con aceite en spray vuelta y vuelta.
* Rellenar con lo que te guste.⁣

## Relleno

Podes rellenarlo con lo que te guste, en esta oportunidad te dejo 2 opciones que quedan muy bien

### Opción 1

* Queso
* Aguacate
* Lechuga
* Tomate cherry
* Semillas de sésamo. ⁣

### Opción 2

* Queso
* Lechuga
* Tomate cherry
* Tomates deshidratados
* Pollo.
⁣
¡⁣Acompañalo con una ensalada verde!
