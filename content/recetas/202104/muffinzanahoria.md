---
title: "Muffins integrales de zanahoria"
date: "2021-04-13 09:26:00"
description: "Deliciosos Muffins de Zanahoria para merendar o desayunar"
type: "Dulce"
tags: ["muffins","postre","cupcake","madalena","zanahoria","merienda","sano","postres"]
category: ["postres","muffins","sano"]
ingredientes: ["3/4 taza de harina leudante","3/4 taza de harina integral","3 Huevos","1 cdita de polvo para hornear", "2 tazas de zanahoria cruda rallada", "1/3 taza de aceite", "2/3 taza de azúcar o miel", "1 cdita de esencia de vainilla", "1 taza de pops de quínoa (Opcional)", "Zanahorias"]
time: "45min"
img: "https://i.postimg.cc/XYPh5QQW/image.png"
---

Deliciosos Muffins de Zanahoria para merendar o desayunar

## Preparación

* Colocar los huevos, el aceite, el azúcar o la miel y la esencia de vainilla en un recipiente.
* Procesar con una minipimer.
* Agregar la zanahoria rallada, mezclar.
* Agregar las harinas, el polvo para hornear y los pops de quínoa, mezclar.
* Esparcir rocío vegetal sobre moldes para muffins.
* Rellenarlos con la preparación.
* Decorarlos con los pops de quínoa y las nueces.
* Cocinar a horno moderado por 30 minutos aprox.

Al tener harina integral y zanahoria aportan mucha fibra.

## Decoracion

* Pops de quínoa (Opcional)
* Nueces
