---
title: "Cake invertida de peras"
date: "2021-04-21 09:26:00"
description: "Deliciosa torta invertida de peras"
type: "Dulce"
tags: ["torta","postre","pera","merienda","sano","postres","desayuno","cake"]
category: ["postres","torta","sano"]
ingredientes: ["2 tazas de avena", "1/2 taza de agua","2 cdas postre de stevia u 8 sobres","2 cdas de leche","2 huevos","1 yogurt de vainilla 160 gr","1 pera grande","Vainilla 1 cdita","2 cditas de polvo de hornear"]
time: "40min"
img: "https://i.postimg.cc/Qt6PjZ7T/image.png"
---

⭐️⭐️🍐CAKE INVERTIDA DE PERAS🍐⭐️⭐️

✅CON AVENA
✅SIN AZÚCAR
✅SIN MANTECA

## Preparación

* Licuar todos los ingredientes menos la pera.
* Cortar y pelar la pera en cuadraditos.
* Colocar la pera en molde de silicona o normal con rocio vegetal. Cubrir con la preparación.
* Hornear por 175 grados por 30 minutos o hasta clavar un cuchillo y salga seco.
