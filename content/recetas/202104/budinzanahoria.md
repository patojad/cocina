---
title: "Budín de zanahorias con nueces"
date: "2021-04-25 19:26:00"
description: "Deliciosos budin de avena, almendras y frutillas"
type: "Dulce"
tags: ["budin","postre","zanahorias","merienda","sano","postres","desayuno","nueces"]
category: ["postres","budin","sano"]
ingredientes: ["1 zanahoria grande o dos chiquitas 🥕", "2 huevos 🥚🥚", "150 gr azúcar o 8 sobres de edulcorante en polvo o 3 cucharadas soperas de edulcorante líquido", "100 ml aceite", "1 taza harina integral", "Nueces la cantidad que quieras!", "1 cucharada de polvo de hornear"]
time: "55min"
img: "https://i.postimg.cc/wTgw6nfp/image.png"
---

Podes remplazar los frutos secos o hacer un mix

## Preparación

* Mezclar los huevos con el aceite y el azúcar.
* Agregar la zanahoria rallada.
* Por último incorporar la harina integral y el polvo de hornear .
* Finalmente la lluvia de #nueces
* Llevar a una budinera y cocinar a 180 grados hasta que colocar un palillo y sale limpio
