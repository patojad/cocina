---
title: "Budin de avena, almendras y frutillas"
date: "2021-04-18 09:26:00"
description: "Deliciosos budin de avena, almendras y frutillas"
type: "Dulce"
tags: ["budin","postre","banana","merienda","sano","postres","desayuno","almendras"]
category: ["postres","budin","sano"]
ingredientes: ["1 huevo","70 g de azúcar mascabo o común","1 cucharadita de esencia de vainilla", "1/4 taza de leche", "1/4 taza de aceite","200g de avena", "1 cucharadita de polvo de hornear", "1 bananaa (una para hacer puré )", "Un puñado de frutillas","8-10 Almendras"]
time: "55min"
img: "https://i.postimg.cc/d1zqXh8S/image.png"
---

También le podes poner mandarinas, bananas, naranjas.

## Preparación

* Mezclar el Huevo, el azúcar y la banana pisada.
* Agregar esencia de vainilla, leche y aceite.
* Luego incorporar la avena y el polvo de hornear.
* Por último colocar la preparación y las rodajas de frutillas y las almendras.

¡Para darme cuenta que esta listo, colocar un cuchillo y debe salir limpio!
