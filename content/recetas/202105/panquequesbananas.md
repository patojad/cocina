---
title: "Panques de banana"
date: "2021-05-02 09:26:00"
description: "Deliciosos panques de banana, avena y canela"
type: "Dulce"
tags: ["panqueque","postre","panqueques","avena","sano","banana","canela","nuez"]
category: ["comida","panqueque","sano"]
ingredientes: ["2 bananas", "1/3 taza de harina integral", "1/3 taza avena 1 huevo", "1 pizca Canela", "1 taza de leche / o leche de soja", "Nuez", "Almendras", "Pasa de uva ( opcional)"]
time: "25min"
img: "https://i.postimg.cc/s2cPx2v9/image.png"
---

Porciones 6 medianos

## Preparación:

* Pizar  una banana
* Mezclar el huevo y la leche
* Luego agregar la harina integral, la avena y  la Canela, revolver bien.
* Agregar la nuez, almendras, pasas de uvas y una banana cortada en rodajas.
* Listo ! Cocinar como panqueques normal .
